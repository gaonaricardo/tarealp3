capitulo_4 = thread-create thread-create2 thread-create3 primes detached critical-section \
			 tsd cleanup cxx-exit job-queue1 job-queue2 job-queue3 spin-condvar condvar thread-pid 

capitulo_5 = shm main mmap-write mmap-read pipe dup2 popen socket-server socket-client \
			 socket-inet

all: $(capitulo_4) $(capitulo_5)
.PHONY: all

# ---------------------------- TARGETS DEL CAPITULO 4 ------------------------------------

thread-create: thread-create.o
	gcc -o bin/capitulo_4/4.1/thread-create obj/capitulo_4/4.1/thread-create.o -lpthread

thread-create.o:
	gcc -o obj/capitulo_4/4.1/thread-create.o -c src/capitulo_4/4.1/thread-create.c

thread-create2: thread-create2.o
	gcc -o bin/capitulo_4/4.2/thread-create2 obj/capitulo_4/4.2/thread-create2.o -lpthread

thread-create2.o:
	gcc -o obj/capitulo_4/4.2/thread-create2.o -c src/capitulo_4/4.2/thread-create2.c

thread-create3: thread-create3.o
	gcc -o bin/capitulo_4/4.3/thread-create3 obj/capitulo_4/4.3/thread-create3.o -lpthread

thread-create3.o:
	gcc -o obj/capitulo_4/4.3/thread-create3.o -c src/capitulo_4/4.3/thread-create3.c

primes: primes.o
	gcc -o bin/capitulo_4/4.4/primes obj/capitulo_4/4.4/primes.o -lpthread

primes.o:
	gcc -o obj/capitulo_4/4.4/primes.o -c src/capitulo_4/4.4/primes.c

detached: detached.o
	gcc -o bin/capitulo_4/4.5/detached obj/capitulo_4/4.5/detached.o -lpthread

detached.o:
	gcc -o obj/capitulo_4/4.5/detached.o -c src/capitulo_4/4.5/detached.c

critical-section: critical-section.o
	gcc -o bin/capitulo_4/4.6/critical-section obj/capitulo_4/4.6/critical-section.o -lpthread

critical-section.o:
	gcc -o obj/capitulo_4/4.6/critical-section.o -c src/capitulo_4/4.6/critical-section.c

tsd: tsd.o
	gcc -o bin/capitulo_4/4.7/tsd obj/capitulo_4/4.7/tsd.o -lpthread

tsd.o:
	gcc -o obj/capitulo_4/4.7/tsd.o -c src/capitulo_4/4.7/tsd.c

cleanup: cleanup.o
	gcc -o bin/capitulo_4/4.8/cleanup obj/capitulo_4/4.8/cleanup.o -lpthread

cleanup.o:
	gcc -o obj/capitulo_4/4.8/cleanup.o -c src/capitulo_4/4.8/cleanup.c

cxx-exit: cxx-exit.o
	g++ -o bin/capitulo_4/4.9/cxx-exit obj/capitulo_4/4.9/cxx-exit.o -lpthread

cxx-exit.o:
	g++ -o obj/capitulo_4/4.9/cxx-exit.o -c src/capitulo_4/4.9/cxx-exit.cpp

job-queue1: job-queue1.o
	gcc -o bin/capitulo_4/4.10/job-queue1 obj/capitulo_4/4.10/job-queue1.o -lpthread

job-queue1.o:
	gcc -o obj/capitulo_4/4.10/job-queue1.o -c src/capitulo_4/4.10/job-queue1.c

job-queue2: job-queue2.o
	gcc -o bin/capitulo_4/4.11/job-queue2 obj/capitulo_4/4.11/job-queue2.o -lpthread

job-queue2.o:
	gcc -o obj/capitulo_4/4.11/job-queue2.o -c src/capitulo_4/4.11/job-queue2.c

job-queue3: job-queue3.o
	gcc -o bin/capitulo_4/4.12/job-queue3 obj/capitulo_4/4.12/job-queue3.o -lpthread

job-queue3.o:
	gcc -o obj/capitulo_4/4.12/job-queue3.o -c src/capitulo_4/4.12/job-queue3.c

spin-condvar: spin-condvar.o
	gcc -o bin/capitulo_4/4.13/spin-condvar obj/capitulo_4/4.13/spin-condvar.o -lpthread

spin-condvar.o:
	gcc -o obj/capitulo_4/4.13/spin-condvar.o -c src/capitulo_4/4.13/spin-condvar.c

condvar: condvar.o
	gcc -o bin/capitulo_4/4.14/condvar obj/capitulo_4/4.14/condvar.o -lpthread

condvar.o:
	gcc -o obj/capitulo_4/4.14/condvar.o -c src/capitulo_4/4.14/condvar.c 

thread-pid: thread-pid.o
	gcc -o bin/capitulo_4/4.15/thread-pid obj/capitulo_4/4.15/thread-pid.o -lpthread

thread-pid.o:
	gcc -o obj/capitulo_4/4.15/thread-pid.o -c src/capitulo_4/4.15/thread-pid.c

# ----------------------------------------------------------------------------------------

# ---------------------------- TARGETS DEL CAPITULO 5 ------------------------------------

obj5 = obj/capitulo_5
src5 = src/capitulo_5
bin5 = bin/capitulo_5

shm: shm.o
	gcc -o $(bin5)/5.1/shm $(obj5)/5.1/shm.o

shm.o:
	gcc -o $(obj5)/5.1/shm.o -c  src/capitulo_5/5.1/shm.c 

req1 = $(obj5)/5.2/sem_all_deall.o
req2 = $(obj5)/5.3/sem_init.o
req3 = $(obj5)/5.4/sem_pv.o
req4 = $(obj5)/5.4/main.o

main: sem_all_deall.o sem_init.o sem_pv.o main.o
	gcc -o $(bin5)/5.4/main $(req1) $(req2) $(req3) $(req4)

sem_all_deall.o:
	gcc -o $(obj5)/5.2/sem_all_deall.o -c src/capitulo_5/5.2/sem_all_deall.c -I src/capitulo_5/5.4

sem_init.o:
	gcc -o $(obj5)/5.3/sem_init.o -c src/capitulo_5/5.3/sem_init.c -I src/capitulo_5/5.4 

sem_pv.o:
	gcc -o $(obj5)/5.4/sem_pv.o -c src/capitulo_5/5.4/sem_pv.c -I src/capitulo_5/5.4 

main.o: 
	gcc -o $(obj5)/5.4/main.o -c src/capitulo_5/5.4/main.c -I src/capitulo_5/5.4 

mmap-write: mmap-write.o
	gcc -o $(bin5)/5.5/mmap-write $(obj5)/5.5/mmap-write.o

mmap-write.o:
	gcc -o $(obj5)/5.5/mmap-write.o -c $(src5)/5.5/mmap-write.c

mmap-read: mmap-read.o
	gcc -o $(bin5)/5.6/mmap-read $(obj5)/5.6/mmap-read.o

mmap-read.o:
	gcc -o $(obj5)/5.6/mmap-read.o -c $(src5)/5.6/mmap-read.c

pipe: pipe.o
	gcc -o $(bin5)/5.7/pipe $(obj5)/5.7/pipe.o

pipe.o:
	gcc -o $(obj5)/5.7/pipe.o -c $(src5)/5.7/pipe.c

dup2: dup2.o
	gcc -o $(bin5)/5.8/dup2 $(obj5)/5.8/dup2.o

dup2.o:
	gcc -o $(obj5)/5.8/dup2.o -c $(src5)/5.8/dup2.c

popen: popen.o
	gcc -o $(bin5)/5.9/popen $(obj5)/5.9/popen.o

popen.o:
	gcc -o $(obj5)/5.9/popen.o -c $(src5)/5.9/popen.c

socket-server: socket-server.o
	gcc -o $(bin5)/5.10/socket-server $(obj5)/5.10/socket-server.o

socket-server.o:
	gcc -o $(obj5)/5.10/socket-server.o -c $(src5)/5.10/socket-server.c

socket-client: socket-client.o
	gcc -o $(bin5)/5.11/socket-client $(obj5)/5.11/socket-client.o

socket-client.o:
	gcc -o $(obj5)/5.11/socket-client.o -c $(src5)/5.11/socket-client.c

socket-inet: socket-inet.o
	gcc -o $(bin5)/5.12/socket-inet $(obj5)/5.12/socket-inet.o

socket-inet.o:
	gcc -o $(obj5)/5.12/socket-inet.o -c $(src5)/5.12/socket-inet.c


# ----------------------------------------------------------------------------------------

clean:
	rm obj/capitulo_4/*/*.o && rm bin/capitulo_4/*/*
	rm obj/capitulo_5/*/*.o && rm bin/capitulo_5/*/*
