#include "semaphore.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>

#define NUM_CHLD	4
#define ADD			0
#define SUB			1
#define MUL			2
#define DIV			3
#define SHM_SIZE 	sizeof(long int)
#define N 			1000000

extern int errno;

int main(){
	int segment_id, i, semid, status, stat;

	pid_t pid[NUM_CHLD];

	key_t key;

	const int shared_segment_size = SHM_SIZE;

	long int *shared_memory;

	void *posible_error;

	/* make the key */
    if ((key = ftok("main.c", 'R')) == -1) {
        perror("ftok");
        exit(1);
    }
    printf("Successfully generated key: %d\n\n", key);


	if((semid = binary_semaphore_allocation(IPC_PRIVATE, IPC_CREAT | IPC_EXCL)) == -1){
		fprintf(stderr, "Error: %s\n", strerror(errno));
		exit(-1);
	}
	printf("The semaphore has been allocated successfully\n\n");

	if((status = binary_semaphore_initialize(semid)) == -1){
		fprintf(stderr, "Error: %s\n", strerror(errno));
		exit(-1);
	}
	printf("The semaphore has been initialized successfully\n\n");

	/* Allocate a shared memory segment */
	if((segment_id = shmget (IPC_PRIVATE, shared_segment_size, 0644 | IPC_CREAT)) == -1){
		fprintf(stderr, "Error: %s\n", strerror(errno));
		exit(-1);
	}
	printf("The shared memory segment has been allocated successfully\n\n");

	/* Attach the shared memory segment */
	shared_memory = shmat(segment_id, (void *)0, 0);
	if(shared_memory == (long int *)(-1)){
		fprintf(stderr, "Error: %s\n", strerror(errno));
		exit(-1);
	}
	printf("The shared memory segment has been attached successfully\n\n");


	*shared_memory = 0;

	for(i = 0; i < NUM_CHLD; ++i){

		if((pid[i] = fork()) == -1){
			fprintf(stderr, "Error al lanzar proceso: %s\n", strerror(errno));
			exit(-1);
		}else if(pid[i] == 0){
			switch(i){

				case ADD:
					printf("The adder has taken control\n");

					binary_semaphore_wait(semid);
					printf("The adder has locked the semaphore\n");

					for(i = 0; i < N; ++i){
						*shared_memory += 1; 
					}

					sleep(5);

					binary_semaphore_post(semid);
					printf("The adder has unlocked the semaphore\n");

					exit(0);
				break;
				case SUB:
					printf("The substractor has taken control\n");

					binary_semaphore_wait(semid);
					printf("The substractor has locked the semaphore\n");

					for(i = 0; i < N; ++i){
						*shared_memory -= 1; 
					}

					sleep(5);

					binary_semaphore_post(semid);
					printf("The substractor has unlocked the semaphore\n");

					exit(0);
				break;
				case MUL:
					printf("The multiplier has taken control\n");

					binary_semaphore_wait(semid);
					printf("The multiplier has locked the semaphore\n");

					*shared_memory = 1;
					for(i = 0; i < (N/50000); ++i){
						*shared_memory *= 2;
					}

					sleep(5);

					binary_semaphore_post(semid);
					printf("The multiplier has unlocked the semaphore\n");

					exit(0);
				break;
				case DIV:
					printf("The divider has taken control\n");

					binary_semaphore_wait(semid);
					printf("The divider has locked the semaphore\n");

					*shared_memory = 2000000000;
					while(*shared_memory > 1){
						*shared_memory /= 2;
					}

					sleep(5);

					binary_semaphore_post(semid);
					printf("The divider has unlocked the semaphore\n");

					exit(0);
				break;
			}
		}
	}

	for(i = 0; i < NUM_CHLD; ++i){
		pid_t cpid = waitpid(pid[i], &stat, 0);
		if(!WIFEXITED(stat)){
			fprintf(stderr, "Error en waitpid: %s\n", strerror(errno));
			exit(-1);
		}
	}

	if(binary_semaphore_deallocate(semid) == -1){
		fprintf(stderr, "Error: %s\n", strerror(errno));
		exit(-1);
	}
	printf("The semaphore has been deallocated successfully!\n");

	printf("The shared memory value is: %ld\n", *shared_memory);

	if(shmdt (shared_memory) == -1){
		fprintf(stderr, "Error: %s\n", strerror(errno));
		exit(-1);
	}
	printf("The shared memory segment has been detached successfully!\n");

	if(shmctl(segment_id, IPC_RMID, NULL) == -1){
		fprintf(stderr, "Error: %s\n", strerror(errno));
		exit(-1);
	}
	printf("The shared memory segment has been deallocated successfully!\n");

	printf("The parent process has finshed.\n");

	
	return 0;
}
