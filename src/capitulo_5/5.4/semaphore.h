#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
/* We must define union semun ourselves.*/
union semun {
	int val;
	struct semid_ds *buf;
	unsigned short int *array;
	struct seminfo *__buf;
};

/* Obtain a binary semaphore’s ID, allocating if necessary.*/
int binary_semaphore_allocation (key_t key, int sem_flags);

/* Deallocate a binary semaphore. All users must have finished their
use. Returns -1 on failure. */
int binary_semaphore_deallocate (int semid);

/* Initialize a binary semaphore with a value of 1.*/
int binary_semaphore_initialize (int semid);

/* Wait on a binary semaphore. Block until the semaphore 
value is positive, then decrement it by 1. */
int binary_semaphore_wait (int semid);

/* Post to a binary semaphore: increment its value by 1.
This returns immediately. */
int binary_semaphore_post (int semid);
