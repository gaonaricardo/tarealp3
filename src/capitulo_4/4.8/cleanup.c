#include <malloc.h>
#include <pthread.h>
/* Allocate a temporary buffer.*/

#define BUFF_SIZE	1024

void* allocate_buffer (size_t size)
{
	return malloc (size);
}
/* Deallocate a temporary buffer.
*/
void deallocate_buffer (void* buffer)
{
	free (buffer);
	printf("Buffer deallocated successfully!\n");
}

/*
	Devuelve la cantidad de primos relativos de num que son
	menores a el
*/
int phi(int num){
	int a, b, phi_val = 0, i;

	if(num == 0 || num == 1)
		return num;

	for(i = 1; i < num; ++i){
		a = num;
		b = i;
		while(a != b){
			if(a > b)
				a = a - b;
			else
				b = b - a;
		}
		if(a == 1)
			phi_val++;
	}
	return phi_val;
}

void* do_some_work (void *arg)
{
	/* Allocate a temporary buffer.*/
	void* temp_buffer = allocate_buffer (BUFF_SIZE);
	int codError = -1;

	if(!temp_buffer)
		pthread_exit(&codError);

	printf("Buffer allocated successfully!\n");

	/* Register a cleanup handler for this buffer, to deallocate it in
	case the thread exits or is cancelled. */
	pthread_cleanup_push (deallocate_buffer, temp_buffer);
	/* Do some work here that might call pthread_exit or might Because
	cancelled... */

	int *lista_numeros = (int *) temp_buffer, n = BUFF_SIZE / sizeof(int), i = 0;

	while(1){
		lista_numeros[i] = phi(i);
		printf("phi(%d) = %d\n", i, lista_numeros[i]);

		if(i++ == n)			// Si se alcanza el limite del buffer
			pthread_exit(0);	// Se termina la ejecucion del hilo
	}

	/* Unregister the cleanup handler. Because we pass a nonzero value,
	this actually performs the cleanup by calling
	deallocate_buffer. */
	pthread_cleanup_pop (1);

	return NULL;
}

int main(){
	pthread_t thread;
	pthread_create(&thread, NULL, &do_some_work, NULL);
	pthread_join(thread, NULL);
	return 0;
}

