#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define TAM_MAX 100

struct job {
/* Link field for linked list.*/
	struct job* next;
/* Other fields describing work to be done... */
	const char* filename;
	int reverse;
};

typedef struct job job;
/* A linked list of pending jobs.*/
job* job_queue = NULL;

/* A mutex protecting job_queue. */
pthread_mutex_t job_queue_mutex = PTHREAD_MUTEX_INITIALIZER;

void process_job(job*);
void add_job(const char *, int);
job* create_job(const char *, int);
void* thread_function(void*);
void merge(int*, int, int, int);
void merge_sort(int*, int, int);
void create_files(int n);

int main(){
	//Add jobs to the queue
	create_files(5);

	pthread_t thread1_id, thread2_id;

	add_job("file1.txt", 0);
	add_job("file2.txt", 1);
	add_job("file3.txt", 0);
	add_job("file4.txt", 1);
	add_job("file5.txt", 0);

	pthread_create(&thread1_id, NULL, &thread_function, NULL);
	pthread_create(&thread2_id, NULL, &thread_function, NULL);
	pthread_join(thread1_id, NULL);
	pthread_join(thread2_id, NULL);

	return 0;
}

void create_files(int n){
	char filename[20] = "";
	FILE *fp = NULL;
	int i, count, j;

	for(i = 1; i <= n; ++i){
		srand(time(NULL)*(i+2));
		sprintf(filename, "file%d.txt", i);
		fp = fopen(filename, "w");
		count = rand() % TAM_MAX;
		for(j = 0; j < count-1; ++j){
			fprintf(fp, "%d\n", rand() % 1000);
		}
		fprintf(fp, "%d", rand() % 1000);
		fclose(fp);
	}
}

job* create_job(const char *filename, int reverse){
	job *new_job = malloc(sizeof(job));

	new_job->next = NULL;
	new_job->filename = filename;
	new_job->reverse = reverse;

	return new_job;
}

void add_job(const char *filename, int reverse){
	pthread_mutex_lock(&job_queue_mutex);
	job* new_job = create_job(filename, reverse);
	new_job->next = job_queue;
	job_queue = new_job;
	pthread_mutex_unlock(&job_queue_mutex);
}

void process_job(job* next_job){
	FILE *fp = fopen(next_job->filename, "r");
	int c, count = 0, i;
	int *array = NULL;

	for (c = getc(fp); c != EOF; c = getc(fp)) 
        if (c == '\n') 
            count = count + 1; 

	array = (int *)malloc(sizeof(int) * count);

	rewind(fp);

	for(i = 0; i < count; ++i)
		fscanf(fp,"%d", &array[i]);

	merge_sort(array, 0, count-1);

	fclose(fp);

	fp = fopen(next_job->filename, "w");

	if(next_job->reverse){
		for(i = count-1; i >= 0; --i)
			fprintf(fp, "%d\n", array[i]);		
	}else{
		for(i = 0; i < count; ++i)
			fprintf(fp, "%d\n", array[i]);
	}

	free(array);
	fclose(fp);
}

// merge function for merging two parts 
void merge(int* a, int low, int mid, int high) 
{ 
    int* left = malloc(sizeof(int) * (mid - low + 1)); 
    int* right = malloc(sizeof(int) * (high - mid)); 
  
    // n1 is size of left part and n2 is size 
    // of right part 
    int n1 = mid - low + 1, n2 = high - mid, i, j; 
  
    // storing values in left part 
    for (i = 0; i < n1; i++) 
        left[i] = a[i + low]; 
  
    // storing values in right part 
    for (i = 0; i < n2; i++) 
        right[i] = a[i + mid + 1]; 
  
    int k = low; 
    i = j = 0; 
  
    // merge left and right in ascending order 
    while (i < n1 && j < n2) { 
        if (left[i] <= right[j]) 
            a[k++] = left[i++]; 
        else
            a[k++] = right[j++]; 
    } 
  
    // insert remaining values from left 
    while (i < n1) { 
        a[k++] = left[i++]; 
    } 
  
    // insert remaining values from right 
    while (j < n2) { 
        a[k++] = right[j++]; 
    }

    free(left);
    free(right); 
} 
  
// merge sort function 
void merge_sort(int *a, int low, int high) 
{ 
    // calculating mid point of array 
    int mid = low + (high - low) / 2; 
    if (low < high) { 
  
        // calling first half 
        merge_sort(a, low, mid); 
  
        // calling second half 
        merge_sort(a, mid + 1, high); 
  
        // merging the two halves 
        merge(a, low, mid, high); 
    } 
}

/*Process queued jobs until the queue is empty.*/
void* thread_function (void* arg)
{
	while (1) {
		job* next_job;
		/* Lock the mutex on the job queue. */
		pthread_mutex_lock (&job_queue_mutex);
		/* Now it’s safe to check if the queue is empty. */
		if (job_queue == NULL)
			next_job = NULL;
		else {
			/* Get the next available job. */
			next_job = job_queue;
			/* Remove this job from the list. */
			job_queue = job_queue->next;
		}
		/* Unlock the mutex on the job queue because we’re done with the
		queue for now. */
		pthread_mutex_unlock (&job_queue_mutex);
		/* Was the queue empty? If so, end the thread.*/
		if (next_job == NULL)
			break;

		/* Carry out the work. */
		process_job (next_job);
		/* Clean up. */
		free (next_job);
	}
	return NULL;
}
