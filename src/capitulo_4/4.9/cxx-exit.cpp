#include <pthread.h>
#include <iostream>

#define MAX 20

using namespace std;

class ThreadExitException
{
public:
	/* Create an exception-signaling thread exit with RETURN_VALUE.*/
	ThreadExitException (void* return_value) : thread_return_value_ (return_value)
	{

	}
	/* Actually exit the thread, using the return value provided in the
	constructor. */
	void* DoThreadExit ()
	{
		pthread_exit (thread_return_value_);
	}
private:
	/* The return value that will be used when exiting the thread.*/
	void* thread_return_value_;
};

int phi(int num){
	int a, b, phi_val = 0, i;

	if(num == 0 || num == 1)
		return num;

	for(i = 1; i < num; ++i){
		a = num;
		b = i;
		while(a != b){
			if(a > b)
				a = a - b;
			else
				b = b - a;
		}
		if(a == 1)
			phi_val++;
	}
	return phi_val;
}

void do_some_work ()
{
	int i = 1;
	while (1) {
	/* Do some useful things here...*/
		
		cout << "phi(" << i << ") = " << phi(i) << "\n";
		
		if (i++ == MAX)
			throw ThreadExitException (/* thread’s return value = */ NULL);
	}
}

void* thread_function (void*)
{
	try {
		do_some_work ();
	}
	catch (ThreadExitException ex) {
		/* Some function indicated that we should exit the thread.*/
		ex.DoThreadExit ();
	}
	return NULL;
}

int main(){
	pthread_t thread_id;
	pthread_create(&thread_id, NULL, &thread_function, NULL);
	pthread_join(thread_id, NULL);
	return 0;
}
